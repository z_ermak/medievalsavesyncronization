﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MedievalSaveSync
{
    public class Player
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int NumberInTurn { get; set; }
        public int GameId { get; set; }
        public int UserId { get; set; }

        public Player()
        {

        }
        public Player(int id)
        {
           
            
        }

        public Player(string name, int numberInTurn, int gameId, int userId)
        {
            Name = name;
            NumberInTurn = numberInTurn;
            GameId = gameId;
            UserId = userId;
        }
    }
}
