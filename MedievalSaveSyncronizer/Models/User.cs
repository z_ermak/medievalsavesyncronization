﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace MedievalSaveSync
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public static User GetUser(string Name)
        {
            return  WebLayer.Execute<User>(WebLayer.Methods.GetUser, new { userName = Name })[0];
        }
        public User()
        {
        }

        public User(int id)
        {
            var user = WebLayer.Execute<User>(WebLayer.Methods.GetUser, new { userId = id })[0];
            ID = id;
            Name = user.Name;

        }

        public string GetRushes()
        {
            return WebLayer.Execute(WebLayer.Methods.GetRushes, new { userid = ID });
        }
    }
}
