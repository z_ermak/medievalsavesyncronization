﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Security.Cryptography;
using System.Timers;

namespace MedievalSaveSync
{
    public class Game
    {
        #region Члены
        BackgroundWorker bwCheckSave;
        Statuses status;
        int lastNextTurnUser;
        int nextTurnUserId;
        Timer tmrCheckSave;
        Timer tmrGetRushes;
        #endregion
        #region Делегаты
        public delegate void StatusChangedEventHandler(object sender, StatusChangedEventArgs args);
        public delegate void TextEventHandler(object sender, TextEventArgs args);
        #endregion
        #region События
        /// <summary>
        /// Происходит, когда меняется действие
        /// </summary>
        public event TextEventHandler ActionChanged;
        /// <summary>
        /// Происходит при смене статуса
        /// </summary>
        public event StatusChangedEventHandler StatusChanged;
        public event TextEventHandler UserTurnChanged;
        /// <summary>
        /// Происходит при успешном завершении цикла синхронизации
        /// </summary>
        public event TextEventHandler SyncCompleted;
        /// <summary>
        /// Происходит при получении новых торопилок
        /// </summary>
        public event TextEventHandler RushesGot;
        #endregion
        #region Классы
        public class StatusChangedEventArgs : EventArgs
        {
            private readonly Statuses status;

            public StatusChangedEventArgs(Statuses status)
            {
                this.status = status;
            }

            public Statuses Status
            {
                get { return this.status; }
            }

            public string Text { get; private set; }
        }

        public class TextEventArgs : EventArgs
        {
            private readonly string actionText;

            public TextEventArgs(string text)
            {
                this.actionText = text;
            }

            public string Text
            {
                get
                {
                    return actionText;
                }
            }

        }
        #endregion
        #region Перечисления
        /// <summary>
        /// Статусы игры
        /// </summary>
        public enum Statuses
        {
            Stop = 0,
            WaitingPlayer = 1,
            MyNewTurn = 2,
            ServerNotAvailable = 3,
            NoSaves = 4,
            OnAction = 5,
            SaveUploadError = 6,
            FatalError = 7,
            MyTurn = 8,
            ChangePlayerTurn = 9
        }
        #endregion
        #region Свойства
        public int ID {get; set; }
        public string Name { get; set; }
        public string SaveName { get; set; }
        public string Creator { get; set; }
        public int IsActive { get; set; }
        public int CreatorId { get; set; }
        public Statuses Status
        {
            get
            {
                return status;
            }
            set
            {
                if (Status != value)
                {
                    status = value;
                    OnStatusChanges(this, new StatusChangedEventArgs(status));
                }
            }
        }
        /// <summary>
        /// Полный путь к сейв-файлу
        /// </summary>
        public string SavePath
        {
            get
            {
                return Settings.GetSavePath(SaveName);
            }
        }
        /// <summary>
        /// Имя игрока, который должен ходить следующим
        /// </summary>
        public string NextTurnUserName { private set; get; }
        #endregion
        #region Конструкторы
        public Game(int id)
        {
            ID = id;
            var game = WebLayer.Execute<Game>(WebLayer.Methods.GetGame, new { gameId = id });
            Name = game[0].Name;
            SaveName = game[0].SaveName;
            Creator = game[0].Creator;
            IsActive = game[0].IsActive;
            CreatorId = game[0].CreatorId;

            bwCheckSave = new BackgroundWorker();
            bwCheckSave.DoWork += new DoWorkEventHandler(bwCheckSave_DoWork);
            bwCheckSave.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwCheckSave_RunWorkerCompleted);

            tmrCheckSave = new Timer();
            tmrCheckSave.Elapsed += new ElapsedEventHandler(tmrCheckSave_Tick);

            tmrGetRushes = new Timer(20000);
            tmrGetRushes.Elapsed += new ElapsedEventHandler(tmrGetRushes_Tick);
        }

        public Game()
        {
        }
        #endregion
        #region Статичные методы
        /// <summary>
        /// Получает список игр, в которых участвует пользователь
        /// </summary>
        /// <param name="userId">id пользователя</param>
        /// <returns></returns>
        public static List<Game> GetGames(int userId)
        {
            var games = WebLayer.Execute<Game>(WebLayer.Methods.GetMyGames, new { userId = userId });
            List<Game> myGames = new List<Game>();
            if (games != null)
            {
                foreach (var g in games)
                {
                    myGames.Add(new Game(g.ID));
                }
            }
            return myGames;
        }

        /// <summary>
        /// Создаёт новую игру
        /// </summary>
        /// <param name="gameName">Имя игры</param>
        /// <param name="saveName">имя сейв-файла</param>
        /// <param name="userId">id юзера-создателя</param>
        /// <returns>id новой игры</returns>
        public static int AddGame(string gameName, string saveName, int userId)
        {
            return int.Parse(WebLayer.Execute(WebLayer.Methods.AddNewGame, new { gameName = gameName, saveName = saveName, userId = userId }));
        }
        #endregion
        #region Публичные методы
        /// <summary>
        /// Добавляет пользователя к игре
        /// </summary>
        /// <param name="userName">Имя пользователя</param>
        /// <param name="gameId">id игры</param>
        public void AddUserToGame(string userName)
        {
            WebLayer.Execute(WebLayer.Methods.AddPlayerToGame, new { userName = userName, gameId = ID });
        }

        /// <summary>
        /// Загружает сейв игры на сервер
        /// </summary>
        /// <returns></returns>
        public bool UploadSave()
        {
            return WebLayer.UploadFile(WebLayer.Methods.UploadSave, new { gameId= ID, userid = Settings.UserId}, SavePath);
        }

        /// <summary>
        /// Получает список игроков
        /// </summary>
        /// <returns></returns>
        public List<Player> GetPlayers()
        {
            return WebLayer.Execute<Player>(WebLayer.Methods.GetGamePlayers, new { gameId = ID });
        }

        /// <summary>
        /// Обновляет информацию об игроках
        /// </summary>
        /// <param name="players">игроки текущей игры</param>
        public void UpdatePlayersList(List<Player> players)
        {
            foreach (Player p in players)
            {
                WebLayer.Execute(WebLayer.Methods.UpdatePlayer, new { gameId = ID, userId = p.UserId, numberInTurn = p.NumberInTurn });
            }
        }
        /// <summary>
        /// Запускает синхронизацию сейва для игры
        /// </summary>
        public void StartSync()
        {
            tmrCheckSave_Tick(null, null);
            tmrGetRushes.Enabled = true;
        }

        public void StopSync()
        {
            tmrCheckSave.Enabled = false;
            tmrGetRushes.Enabled = false;
            Status = Statuses.Stop;
        }

        /// <summary>
        /// Запускает проверку сейва
        /// </summary>
        public void Check()
        {
            if (!bwCheckSave.IsBusy)
            {
                bwCheckSave.RunWorkerAsync();
            }
        }
        /// <summary>
        /// Поторапливает игрока, который должен ходить сейчас
        /// </summary>
        /// <returns>Имя поторопленного игрока</returns>
        public string PushRush()
        {
            return WebLayer.Execute(WebLayer.Methods.PushRush, new { gameId = ID});
        }
        #endregion
        #region Методы синхронизации
        /// <summary>
        /// Проверяет сейв 
        /// </summary>
        /// <returns>true - если успешно, иначе false</returns>
        private Statuses CheckSave()
        {
            Statuses status;
            try
            {
                if (WebLayer.ServerCheckAvailable())
                {
                    nextTurnUserId = GetNextTurnUserId();
                    if (nextTurnUserId != Settings.UserId)
                    {
                        //Если сейчас не наш ход
                        if (nextTurnUserId == 0)
                        {
                            //Никто еще не ходил
                            status = Statuses.NoSaves;
                        }
                        else
                        {
                            //Ждём другого игрока
                            status = Statuses.WaitingPlayer;
                            if (lastNextTurnUser != nextTurnUserId)
                            {
                                lastNextTurnUser = nextTurnUserId;
                                //Сменился ход
                                NextTurnUserName = WebLayer.Execute(WebLayer.Methods.GetUserName, new { userId = nextTurnUserId });
                                status = Statuses.ChangePlayerTurn;
                            }
                        }
                    }
                    else
                    {
                        //Если наш ход:
                        status = Statuses.MyNewTurn;
                        lastNextTurnUser = Settings.UserId;
                        //OnUserTurnChanged(this, new TextEventArgs("ТВОЙ ХОД!"));
                        if (!File.Exists(SavePath))
                        {
                            //Скачаем сейв, если его вообще не было
                            RaiseEventActionChanged("Качаем сейв...");
                            WebLayer.GetFile(WebLayer.Methods.GetSave, new { userid = Settings.UserId, gameId = ID }, SavePath);
                        }
                        else
                        {
                            //Если сейв уже есть - проверим
                            if(Convert.ToBoolean(int.Parse(WebLayer.Execute(WebLayer.Methods.CheckMySave, new { hash  = GetHash(SavePath), gameId= ID }))))
                            {
                                //Если равны - значит уже скачали свежую версию - напоминаем,что надо походить
                                status = Statuses.MyTurn;
                            }
                            else
                            {
                                //Если не равны
                                var fi = new FileInfo(SavePath);
                                RaiseEventActionChanged("Сравнение дат сейвов...");
                                if (GetLastSaveDate() > fi.LastWriteTime)
                                {
                                    //На сервере свежее - надо скачать
                                    WebLayer.GetFile(WebLayer.Methods.GetSave, new { gameId = ID }, SavePath);
                                }
                                else
                                {
                                    //У нас свежее -  надо отправить
                                    RaiseEventActionChanged("Загрузка сейва");
                                    if (UploadSave())
                                    {
                                        //Если всё ок - обновится инфа о том, что  ходит следующий игрок
                                        //return CheckSave();
                                    }
                                    else
                                    {
                                        status = Statuses.SaveUploadError;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    status = Statuses.ServerNotAvailable;
                }

            }
            catch (Exception ex)
            {
                status = Statuses.FatalError;
                OnActionChanged(this, new TextEventArgs(ex.Message));
                return status;
            }
            return status;
        }

        /// <summary>
        /// Получает id игрока, который должен ходить следующим
        /// </summary>
        /// <returns>ID игрока, если были ходы. Иначе 0</returns>
        private int GetNextTurnUserId()
        {
            int uid = 0;
            int.TryParse(WebLayer.Execute(WebLayer.Methods.GetNextTurnUserId, new { gameId = ID }), out uid);
            return uid;
        }

        /// <summary>
        /// Получает дату и время последнего сохранения
        /// </summary>
        /// <returns></returns>
        private DateTime GetLastSaveDate()
        {
            return DateTime.Parse(WebLayer.Execute(WebLayer.Methods.GetLastSaveDate, new { gameId = ID }));
        }

        private void bwCheckSave_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = CheckSave();
        }

        private void bwCheckSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Status = (Statuses)e.Result;
            tmrCheckSave.Enabled = Settings.Status == Settings.Statuses.Started;
            OnSyncCompleted(this, new TextEventArgs(DateTime.Now.ToString()));
        }
        #endregion
        #region Вспомогательные методы
        /// <summary>
        /// Вычисляет MD5-хэш
        /// </summary>
        /// <param name="path">Путь к файлу</param>
        /// <returns>MD5-хэш</returns>
        private string GetHash(string path)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(path))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToLowerInvariant();
                }
            }
        }
        #endregion
        #region Обработчики событий
        protected virtual void OnActionChanged(object sender, TextEventArgs e)
        {
            TextEventHandler handler = ActionChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected virtual void OnStatusChanges(object sender, StatusChangedEventArgs e)
        {
            StatusChangedEventHandler handler = StatusChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected virtual void OnUserTurnChanged(object sender, TextEventArgs e)
        {
            TextEventHandler handler = UserTurnChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        private void RaiseEventActionChanged(string actionText)
        {
            OnActionChanged(this, new TextEventArgs(actionText));
        }

        protected virtual void OnSyncCompleted(object sender, TextEventArgs e)
        {
            TextEventHandler handler = SyncCompleted;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        protected virtual void OnRushesGot(object sender, TextEventArgs e)
        {
            TextEventHandler handler = RushesGot;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion
        #region Приватные методы
        private void tmrCheckSave_Tick(object sender, EventArgs e)
        {
            tmrCheckSave.Stop();
            tmrCheckSave.Interval = Settings.CheckInterval * 1000;
            Check();
        }

        private void tmrGetRushes_Tick(object sender, EventArgs e)
        {
            tmrGetRushes.Stop();
            OnRushesGot(this, new TextEventArgs(WebLayer.Execute(WebLayer.Methods.GetRushes, new { userid = Settings.UserId, gameId = ID })));
            tmrGetRushes.Start();
        }
        #endregion
    }
}
