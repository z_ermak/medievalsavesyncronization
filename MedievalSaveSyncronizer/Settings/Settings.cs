﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows.Forms;

namespace MedievalSaveSync
{
    public static class Settings
    {
        /// <summary>
        /// Статусы программы
        /// </summary>
        public enum Statuses
        {
            Stopped = 0,
            Started = 1
        }
        static string AppKeyPath = @"SOFTWARE\MedievalSaveSyncronizer";
        #region Публичные свойства
        /// <summary>
        /// Идентификатор игрока
        /// </summary>
        public static int UserId { get; set; }
        /// <summary>
        /// Адрес сервера
        /// </summary>
        public static string ServerURL { get; set; }
        /// <summary>
        /// Текущий статус работы программы
        /// </summary>
        public static Statuses Status { get; set; }
        /// <summary>
        /// Путь к папке с игрой
        /// </summary>
        public static string GameFolder { get; set; }
        /// <summary>
        /// Путь к файлу запуска игры
        /// </summary>
        public static string StartGamePath
        {
            get
            {
                return GameFolder + @"\mods\retrofit\medieval2_retrofit.bat";
            }
        }
        #endregion 
        /// <summary>
        /// Отключить все нотификации, кроме критической ошибки, уведомлении о ходе и торопилок
        /// </summary>
        public static bool DisableNotification { get; set; }
        /// <summary>
        /// Интервал проверки сейва на сервере
        /// </summary>
        public static int CheckInterval { get; set; }
        /// <summary>
        /// Загружает настройки из реестра
        /// </summary>
        public static bool LoadSettings()
        {
            try
            {
                RegistryKey CurrentUserKey = Registry.CurrentUser;
                var appKey = CurrentUserKey.OpenSubKey(AppKeyPath);
                if (appKey == null)
                {
                    if (FirstRunSetup(CurrentUserKey))
                    {
                        SetStartup();
                        return LoadSettings();
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    Settings.ServerURL = appKey.GetValue(nameof(Settings.ServerURL)).ToString();
                    Settings.UserId = int.Parse(appKey.GetValue(nameof(Settings.UserId)).ToString());
                    Settings.DisableNotification = bool.Parse(appKey.GetValue(nameof(Settings.DisableNotification)).ToString());
                    Settings.CheckInterval = int.Parse(appKey.GetValue(nameof(Settings.CheckInterval)).ToString());
                    Settings.GameFolder = appKey.GetValue(nameof(Settings.GameFolder)).ToString();
                    appKey.Close();
                    return true;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(String.Format("Ошибка при загрузке настроек: {0}", ex.Message), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        /// <summary>
        /// Открывает окно выбора папки
        /// </summary>
        /// <returns>Путь выбранной папки</returns>
        public static string SelectGameFolder()
        {
            string gameFolder = string.Empty;
            while (gameFolder == string.Empty)
            {
                FolderBrowserDialog dlg = new FolderBrowserDialog();
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    gameFolder = dlg.SelectedPath;
                    if (!File.Exists(gameFolder + @"\medieval2.exe"))
                    {
                        MessageBox.Show("Выбран неверный каталог! Надо выбрать папку, где лежит medieval2.exe");
                        gameFolder = string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
            return gameFolder;
        }
        /// <summary>
        /// Сохраняет настройки в реестр
        /// </summary>
        public static void SaveSettings()
        {
            try
            {
                RegistryKey CurrentUserKey = Registry.CurrentUser;
                string subKeyPath = @"SOFTWARE\MedievalSaveSyncronizer";
                var subKey = CurrentUserKey.OpenSubKey(subKeyPath, RegistryKeyPermissionCheck.ReadWriteSubTree);
                subKey.SetValue(nameof(Settings.ServerURL), Settings.ServerURL);
                subKey.SetValue(nameof(Settings.CheckInterval), Settings.CheckInterval);
                subKey.SetValue(nameof(Settings.UserId), Settings.UserId);
                subKey.SetValue(nameof(Settings.DisableNotification), Settings.DisableNotification);
                subKey.SetValue(nameof(Settings.GameFolder), Settings.GameFolder);
                subKey.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        /// <summary>
        /// Прописывает программу в автозагрузку
        /// </summary>
        private static void SetStartup()
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey
                ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            rk.SetValue("Синхронизация сейвов Medieval 2 Total War", Application.ExecutablePath);

        }
        /// <summary>
        /// Запускает настройку параметров перед первым запуском
        /// </summary>
        private static bool FirstRunSetup(RegistryKey сurrentUserKey)
        {
            //первый запуск
            string url = string.Empty;
            while (url == string.Empty)
            {
                url = "92.62.51.64";
                InputBox.ShowInputBox("Первый запуск", "Введите адрес сервера", ref url);
            }
            if (WebLayer.ServerCheckAvailable(url))
            {
                Settings.ServerURL = url;
                string playerName = string.Empty;
                int uid = 0;
                while (playerName == string.Empty || uid == 0)
                {
                    InputBox.ShowInputBox("Первый запуск", "Введите свой ник", ref playerName);
                    playerName = playerName.Trim();
                    if (playerName != string.Empty)
                    {
                        uid = int.Parse(WebLayer.Execute(WebLayer.Methods.RegisterPlayer, new { playerName = playerName }));
                    }
                    if (uid == 0 )
                    {
                        MessageBox.Show(String.Format("Имя \"{0}\" уже занято.", playerName), "Не удалось зарегистрировать игрока", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }

                string gameFolder = string.Empty;
                while (gameFolder == String.Empty)
                {
                    gameFolder = SelectGameFolder();
                }
                var syncKey = сurrentUserKey.CreateSubKey(AppKeyPath);
                syncKey.SetValue(nameof(Settings.UserId), uid);
                syncKey.SetValue(nameof(Settings.ServerURL), url);
                syncKey.SetValue(nameof(Settings.GameFolder), gameFolder);

                syncKey.SetValue(nameof(Settings.DisableNotification), false);
                syncKey.SetValue(nameof(Settings.CheckInterval), 300);
                syncKey.Close();
                return true;
            }
            else
            {
                MessageBox.Show(String.Format("Сервер {0} недоступен. Продолжение невозможно." , url), "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Error );
                return false;
            }
        }

        public static string GetSavePath(string saveName)
        {
            return GameFolder + @"\mods\retrofit\saves\" + saveName;
        }
    }
}
