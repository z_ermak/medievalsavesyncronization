﻿namespace MedievalSaveSync
{
    partial class GameControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNum = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.btnRush = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNum
            // 
            this.lblNum.AutoSize = true;
            this.lblNum.Location = new System.Drawing.Point(0, 1);
            this.lblNum.Name = "lblNum";
            this.lblNum.Size = new System.Drawing.Size(16, 13);
            this.lblNum.TabIndex = 0;
            this.lblNum.Text = "1.";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(22, 1);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(70, 13);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Игра друзей";
            this.lblName.TextChanged += new System.EventHandler(this.lblName_TextChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(134, 1);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(98, 13);
            this.lblStatus.TabIndex = 2;
            this.lblStatus.Text = "Ходит игрок Иван";
            this.lblStatus.TextChanged += new System.EventHandler(this.lblStatus_TextChanged);
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Location = new System.Drawing.Point(247, 1);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(35, 13);
            this.lblDateTime.TabIndex = 3;
            this.lblDateTime.Text = "label1";
            this.lblDateTime.TextChanged += new System.EventHandler(this.lblDateTime_TextChanged);
            // 
            // btnRush
            // 
            this.btnRush.Location = new System.Drawing.Point(314, -2);
            this.btnRush.Name = "btnRush";
            this.btnRush.Size = new System.Drawing.Size(23, 18);
            this.btnRush.TabIndex = 4;
            this.btnRush.Text = "Т";
            this.btnRush.UseVisualStyleBackColor = true;
            this.btnRush.Click += new System.EventHandler(this.btnRush_Click);
            // 
            // GameControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRush);
            this.Controls.Add(this.lblDateTime);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblNum);
            this.Name = "GameControl";
            this.Size = new System.Drawing.Size(350, 18);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNum;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Button btnRush;
    }
}
