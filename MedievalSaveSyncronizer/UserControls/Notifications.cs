﻿using System;
using System.Drawing;
using System.Media;
using Tulpep.NotificationWindow;

namespace MedievalSaveSync
{
    public class Notifications
    {
        public event EventHandler PopupClicked;
        /// <summary>
        /// Показывается всплывающее окно в правом низу экрана
        /// </summary>
        /// <param name="text">Текст нотификации</param>
        /// <param name="title">Заголовок</param>
        /// <param name="time">Время отображения</param>
        public void ShowPopup(string text, string title, int time = 5000)
        {
            PopupNotifier pn = new PopupNotifier();
            pn.ContentText = text;
            pn.TitleText = title;
            pn.BodyColor = Color.LightSkyBlue;
            pn.TitleColor = Color.DarkBlue;
            pn.Delay = time;
            pn.Click += new EventHandler(OnPopupClick);
            pn.Popup();
            PlaySound();
        }

        protected virtual void OnPopupClick(object sender, EventArgs e)
        {
            EventHandler handler = PopupClicked;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        /// <summary>
        /// Воспроизводит звук
        /// </summary>
        private void PlaySound()
        {
            var sound = Properties.Resources.sound;
            SoundPlayer player = new SoundPlayer(sound);
            player.Play();
        }
    }
}
