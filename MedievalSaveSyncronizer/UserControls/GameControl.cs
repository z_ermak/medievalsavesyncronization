﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace MedievalSaveSync
{
    public partial class GameControl : UserControl
    {
        private Game Game { get; set; }
        Notifications notifications;
        private int gameNumber;
        public GameControl(int num, Game game)
        {
            InitializeComponent();
            Game = game;
            gameNumber = num;
            AlignElemnts();
            Game.ActionChanged += new Game.TextEventHandler(OnActionChanged);
            Game.StatusChanged += new Game.StatusChangedEventHandler(OnStatusChanged);
            Game.UserTurnChanged += new Game.TextEventHandler(OnUserTurnChanged);
            Game.SyncCompleted += new Game.TextEventHandler(OnSyncCompleted);
            Game.RushesGot += new Game.TextEventHandler(OnRushesGot);

            notifications = new Notifications();
        }

        private void AlignElemnts()
        {
            lblNum.Text = String.Format("{0}.", gameNumber);
            lblNum.AutoSize = true;
            lblName.Left = lblNum.Left + lblNum.Width + 5;
            lblName.Text = Game.Name;
            lblNum.AutoSize = true;
            lblStatus.Left = lblName.Left + lblName.Width + 5;
            lblDateTime.Left = lblStatus.Left + lblStatus.Width + 5;
            btnRush.Left = lblDateTime.Left + lblDateTime.Width + 5;
        }

        private void OnActionChanged(object sender, Game.TextEventArgs e)
        {
            //lblStatus.Text = e.ToString();
        }

        private void OnUserTurnChanged(object sender, Game.TextEventArgs e)
        {
            if (Game.Status == Game.Statuses.WaitingPlayer)
            {
                string nextTurnUser = "Ждём. Ходит " + Game.NextTurnUserName;
                lblStatus.Text = nextTurnUser;
                notifications.ShowPopup(nextTurnUser, "Ход игрока");
            }
        }

        private void OnStatusChanged(object sender, Game.StatusChangedEventArgs e)
        {
            switch (e.Status)
            {
                case Game.Statuses.WaitingPlayer:
                    {
                        //string nextTurnUser = "Ждём. Ходит " + Game.NextTurnUserName;
                        //lblStatus.Text = nextTurnUser;
                        //notifications.ShowPopup(nextTurnUser, "Ход игрока");
                        break;
                    }
                case Game.Statuses.ChangePlayerTurn:
                    {
                        string nextTurnUser = "Ждём. Ходит " + Game.NextTurnUserName;
                        SetStatus(nextTurnUser);
                        ShowPopup(nextTurnUser, "Ход игрока");
                        break;
                    }
                case Game.Statuses.MyNewTurn:
                    {
                        SetStatus("Твой ход!");
                        ShowPopup("ТВОЙ ХОД!", String.Format("Новый раунд в игре {0}", Game.Name));
                        break;
                    }
                case Game.Statuses.ServerNotAvailable:
                    {
                        SetStatus("Сервер недоступен");
                        ShowPopup("СЕРВЕР НЕДОСТУПЕН! Синхронизация остановлена. Включен режим проверки соединения", "Ошибка");
                        break;
                    }
                case Game.Statuses.NoSaves:
                    {
                        SetStatus("Нет первого сейва");
                        break;
                    }
                case Game.Statuses.OnAction:
                    {
                        //Вообще-то этого статуса не должно быть здесь в результате
                        //lblStatus.Text = "В процессе. Похоже что-то зависло";
                        //notifications.ShowPopup("Скорее всего что-то не так", "Подозрительно в процессе");
                        break;
                    }
                case Game.Statuses.SaveUploadError:
                    {
                        SetStatus("Ошибка загрузки сейва");
                        ShowPopup("СЕЙВ НЕ ЗАГРУЖЕН", "ОШИБКА");
                        break;
                    }
                case Game.Statuses.FatalError:
                    {
                        SetStatus("Фатальная ошибка");
                        break;
                    }
                case Game.Statuses.MyTurn:
                    {
                        SetStatus("Твой ход!");
                        break;
                    }
                case Game.Statuses.Stop:
                    {
                        SetStatus("Синх. остановлена");
                        break;
                    }

            }
        }

        private void OnSyncCompleted(object sender, Game.TextEventArgs e)
        {
            lblDateTime.Invoke(new MethodInvoker(() => { lblDateTime.Text = e.Text; }));
        }

        private void OnRushesGot(object sender, Game.TextEventArgs e)
        {
            if (e.Text != String.Empty)
            {
                ShowPopup(e.Text, String.Format("Игра {0}", Game.Name));
            }
        }
        
        private void SetStatus(string text)
        {
            lblStatus.Invoke(new MethodInvoker(() => { lblStatus.Text = text; }));
        }

        private void ShowPopup(string text, string caption = "")
        {
            this.Invoke(new MethodInvoker(() => { notifications.ShowPopup(text, caption); }));
        }

        private void lblName_TextChanged(object sender, EventArgs e)
        {
            AlignElemnts();
        }

        private void lblStatus_TextChanged(object sender, EventArgs e)
        {
            AlignElemnts();
        }

        private void lblDateTime_TextChanged(object sender, EventArgs e)
        {
            AlignElemnts();
        }

        private void btnRush_Click(object sender, EventArgs e)
        {
            Game.PushRush();
        }
    }
}
