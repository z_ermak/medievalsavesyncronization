﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MedievalSaveSync
{
    public partial class CheckMedievalSave : Form
    {
        Notifications notifications;
        List<Game> myGames;
        User user;

        private bool allowClose;

        public CheckMedievalSave()
        {
            InitializeComponent();
            niTray.ContextMenuStrip = new ContextMenuStrip();
            niTray.ContextMenuStrip.Items.Add("Выход", null, OnExitTrayMenuClick);

            notifications = new Notifications();
            notifications.PopupClicked += new EventHandler(OnPopupClicked);

            if (!Settings.LoadSettings())
            {
                MessageBox.Show("Не удалось загрузить настройки", "Ошибка при загрузке настроек", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }

            user = new User(Settings.UserId);
            tmrCheckMyGames.Enabled = true;
        }
        /// <summary>
        /// Запуск синхронизации
        /// </summary>
        private void Start()
        {
            Settings.Status = Settings.Statuses.Started;
            tsbStop.Enabled = true;
            tsbStart.Enabled = false;
            foreach(var game in myGames)
            {
                game.StartSync();
            }

        }
        /// <summary>
        /// Остановка синхронизации
        /// </summary>
        private void Stop()
        {
            Settings.Status = Settings.Statuses.Stopped;
            tsbStop.Enabled = false;
            tsbStart.Enabled = true;
            foreach (var game in myGames)
            {
                game.StopSync();
            }
        }
        
        private void CheckMedievalSave_Load(object sender, EventArgs e)
        {         
            niTray.Visible = true;
            this.ShowInTaskbar = false;
            //tsbStart.PerformClick();
            LoadMyGames();
        }

        private void CheckMedievalSave_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                niTray.Visible = true;
            }
        }

        private void niTray_DoubleClick(object sender, EventArgs e)
        {
            Show();
            this.WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
        }

        private void OnPopupClick(object sender, EventArgs e)
        {
            niTray_DoubleClick(null, null);
        }

        private void tsSettings_Click(object sender, EventArgs e)
        {
            frmSettings frm = new frmSettings();
            frm.ShowDialog();
        }

        private void tsbStart_Click(object sender, EventArgs e)
        {
            Start();
        }

        private void tsbStop_Click(object sender, EventArgs e)
        {
            Stop();
        }

        private void tsbStartGame_Click(object sender, EventArgs e)
        {
            ProcessStartInfo processInfo;
            Process process;
            processInfo = new ProcessStartInfo();
            processInfo.FileName = "cmd.exe";
            processInfo.RedirectStandardInput = true;
            processInfo.UseShellExecute = false;
            process = Process.Start(processInfo);

            using (StreamWriter sw = process.StandardInput)
            {
                if (sw.BaseStream.CanWrite)
                {
                    sw.WriteLine(Settings.StartGamePath[0] + @":");
                    sw.WriteLine("cd \""+Path.GetDirectoryName(Settings.StartGamePath)+"\"");
                    sw.WriteLine("\""+Settings.StartGamePath+"\"");
                }
            }

            this.Hide();
        }

        private void tmrCheckConnection_Tick(object sender, EventArgs e)
        {
            tmrCheckConnection.Stop();
            if (WebLayer.ServerCheckAvailable())
            {
                
                notifications.ShowPopup("Соединение с сервером установлено. Запуск синхронизации", "Всё ок");
                tsbStart.PerformClick();         
            }
            else
            {
                tmrCheckConnection.Start();
            }
        }

        private void tmrGetRushes_Tick(object sender, EventArgs e)
        {
            string rushes = user.GetRushes();
            if (rushes != string.Empty)
            {
                notifications.ShowPopup(rushes, "Тебя торопят!");
            }
        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            foreach (var game in myGames)
            {
                game.Check();
            }
        }
      
        private void OnPopupClicked(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            ShowInTaskbar = true;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (!allowClose)
            {
                this.Hide();
                e.Cancel = true;
            }
            base.OnFormClosing(e);
        }

        private void OnExitTrayMenuClick(object sender, EventArgs e)
        {
            allowClose = true;
            Application.Exit();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            var fGamesSettings = new frmGamesSettings();
            fGamesSettings.ShowDialog();
        }

        private string LoadMyGames()
        {
            var games = Game.GetGames(Settings.UserId);
            string gamesInfo = string.Empty;
            if (games.Count>0)
            {
                bool reloadGames = false;
                if (myGames == null || myGames.Count != games.Count)
                {
                    reloadGames = true;
                }
                else
                {
                    foreach(var game in myGames)
                    {
                        if (!games.Any(g => g.ID == game.ID))
                        {
                            reloadGames = true;
                            break;
                        }
                    }
                }

                if (reloadGames)
                {
                    gpMyGames.Controls.Clear();
                    int i = 1;
                    foreach (var game in games)
                    {
                        var uGame = new GameControl(i, game);
                        uGame.Left = 1;
                        uGame.Top = uGame.Height * i;
                        gpMyGames.Controls.Add(uGame);
                        i++;

                        if (myGames== null || !myGames.Any(g => g.ID == game.ID))
                        {
                            gamesInfo += String.Format("Вас добавили в игру {0} \r\n", game.Name);
                        }
                    }

                    if (myGames != null)
                    {
                        foreach (var game in myGames)
                        {
                            if (!games.Any(g => g.ID == game.ID))
                            {
                                gamesInfo += String.Format("Вас удалили из игры {0} \r\n", game.Name);
                            }
                        }
                    }
                    myGames = games;
                }
            }
            return gamesInfo;
        }

        private void tmrCheckMyGames_Tick(object sender, EventArgs e)
        {
            tmrCheckMyGames.Enabled = false;
            var gamesInfo = LoadMyGames();
            if (gamesInfo != string.Empty)
            {
                notifications.ShowPopup(gamesInfo, "Информация об играх");
            }
            tmrCheckMyGames.Enabled = true;
        }
    }
}
