﻿namespace MedievalSaveSync
{
    partial class CheckMedievalSave
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckMedievalSave));
            this.niTray = new System.Windows.Forms.NotifyIcon(this.components);
            this.tsMenu = new System.Windows.Forms.ToolStrip();
            this.tsSettings = new System.Windows.Forms.ToolStripButton();
            this.tsbStart = new System.Windows.Forms.ToolStripButton();
            this.tsbStop = new System.Windows.Forms.ToolStripButton();
            this.tsbStartGame = new System.Windows.Forms.ToolStripButton();
            this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbGameConfig = new System.Windows.Forms.ToolStripButton();
            this.tmrCheckConnection = new System.Windows.Forms.Timer(this.components);
            this.tmrShowPopupMyTurm = new System.Windows.Forms.Timer(this.components);
            this.gpMyGames = new System.Windows.Forms.GroupBox();
            this.tmrCheckMyGames = new System.Windows.Forms.Timer(this.components);
            this.tsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // niTray
            // 
            this.niTray.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.niTray.BalloonTipText = "Твой ход";
            this.niTray.BalloonTipTitle = "Ходи";
            this.niTray.Icon = ((System.Drawing.Icon)(resources.GetObject("niTray.Icon")));
            this.niTray.Text = "Проверка сейва";
            this.niTray.Visible = true;
            this.niTray.DoubleClick += new System.EventHandler(this.niTray_DoubleClick);
            // 
            // tsMenu
            // 
            this.tsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSettings,
            this.tsbStart,
            this.tsbStop,
            this.tsbStartGame,
            this.tsbRefresh,
            this.tsbGameConfig});
            this.tsMenu.Location = new System.Drawing.Point(0, 0);
            this.tsMenu.Name = "tsMenu";
            this.tsMenu.Size = new System.Drawing.Size(310, 25);
            this.tsMenu.TabIndex = 8;
            this.tsMenu.Text = "toolStrip1";
            // 
            // tsSettings
            // 
            this.tsSettings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsSettings.Image = ((System.Drawing.Image)(resources.GetObject("tsSettings.Image")));
            this.tsSettings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSettings.Name = "tsSettings";
            this.tsSettings.Size = new System.Drawing.Size(23, 22);
            this.tsSettings.Text = "Настройка";
            this.tsSettings.Click += new System.EventHandler(this.tsSettings_Click);
            // 
            // tsbStart
            // 
            this.tsbStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbStart.Image = ((System.Drawing.Image)(resources.GetObject("tsbStart.Image")));
            this.tsbStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStart.Name = "tsbStart";
            this.tsbStart.Size = new System.Drawing.Size(23, 22);
            this.tsbStart.Text = "Запустить";
            this.tsbStart.Click += new System.EventHandler(this.tsbStart_Click);
            // 
            // tsbStop
            // 
            this.tsbStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbStop.Image = ((System.Drawing.Image)(resources.GetObject("tsbStop.Image")));
            this.tsbStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStop.Name = "tsbStop";
            this.tsbStop.Size = new System.Drawing.Size(23, 22);
            this.tsbStop.Text = "Остановить";
            this.tsbStop.Click += new System.EventHandler(this.tsbStop_Click);
            // 
            // tsbStartGame
            // 
            this.tsbStartGame.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbStartGame.Image = ((System.Drawing.Image)(resources.GetObject("tsbStartGame.Image")));
            this.tsbStartGame.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStartGame.Name = "tsbStartGame";
            this.tsbStartGame.Size = new System.Drawing.Size(23, 22);
            this.tsbStartGame.Text = "toolStripButton1";
            this.tsbStartGame.ToolTipText = "Запустить игру";
            this.tsbStartGame.Click += new System.EventHandler(this.tsbStartGame_Click);
            // 
            // tsbRefresh
            // 
            this.tsbRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
            this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefresh.Name = "tsbRefresh";
            this.tsbRefresh.Size = new System.Drawing.Size(23, 22);
            this.tsbRefresh.Text = "Обновить";
            this.tsbRefresh.Click += new System.EventHandler(this.tsbRefresh_Click);
            // 
            // tsbGameConfig
            // 
            this.tsbGameConfig.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbGameConfig.Image = ((System.Drawing.Image)(resources.GetObject("tsbGameConfig.Image")));
            this.tsbGameConfig.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGameConfig.Name = "tsbGameConfig";
            this.tsbGameConfig.Size = new System.Drawing.Size(23, 22);
            this.tsbGameConfig.Text = "Настройка игр";
            this.tsbGameConfig.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // tmrCheckConnection
            // 
            this.tmrCheckConnection.Interval = 120000;
            this.tmrCheckConnection.Tick += new System.EventHandler(this.tmrCheckConnection_Tick);
            // 
            // gpMyGames
            // 
            this.gpMyGames.Location = new System.Drawing.Point(0, 28);
            this.gpMyGames.Name = "gpMyGames";
            this.gpMyGames.Size = new System.Drawing.Size(310, 92);
            this.gpMyGames.TabIndex = 9;
            this.gpMyGames.TabStop = false;
            this.gpMyGames.Text = "Мои игры";
            // 
            // tmrCheckMyGames
            // 
            this.tmrCheckMyGames.Interval = 5000;
            this.tmrCheckMyGames.Tick += new System.EventHandler(this.tmrCheckMyGames_Tick);
            // 
            // CheckMedievalSave
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(310, 124);
            this.Controls.Add(this.gpMyGames);
            this.Controls.Add(this.tsMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CheckMedievalSave";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Синхронизация сейва Medieval TW";
            this.Load += new System.EventHandler(this.CheckMedievalSave_Load);
            this.Resize += new System.EventHandler(this.CheckMedievalSave_Resize);
            this.tsMenu.ResumeLayout(false);
            this.tsMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.NotifyIcon niTray;
        private System.Windows.Forms.ToolStrip tsMenu;
        private System.Windows.Forms.ToolStripButton tsSettings;
        private System.Windows.Forms.ToolStripButton tsbStart;
        private System.Windows.Forms.ToolStripButton tsbStop;
        private System.Windows.Forms.ToolStripButton tsbStartGame;
        private System.Windows.Forms.Timer tmrCheckConnection;
        private System.Windows.Forms.ToolStripButton tsbRefresh;
        private System.Windows.Forms.Timer tmrShowPopupMyTurm;
        private System.Windows.Forms.ToolStripButton tsbGameConfig;
        private System.Windows.Forms.GroupBox gpMyGames;
        private System.Windows.Forms.Timer tmrCheckMyGames;
    }
}

