﻿namespace MedievalSaveSync
{
    partial class frmGamesSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGamesSettings));
            this.dgvMyGames = new System.Windows.Forms.DataGridView();
            this.lblMyGames = new System.Windows.Forms.Label();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tsbAddGame = new System.Windows.Forms.ToolStripButton();
            this.tsbDeleteGame = new System.Windows.Forms.ToolStripButton();
            this.tsbConfigGame = new System.Windows.Forms.ToolStripButton();
            this.tsbSaves = new System.Windows.Forms.ToolStripButton();
            this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbUploadFirstSave = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyGames)).BeginInit();
            this.tsMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMyGames
            // 
            this.dgvMyGames.AllowUserToAddRows = false;
            this.dgvMyGames.AllowUserToDeleteRows = false;
            this.dgvMyGames.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvMyGames.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMyGames.Location = new System.Drawing.Point(7, 50);
            this.dgvMyGames.Name = "dgvMyGames";
            this.dgvMyGames.ReadOnly = true;
            this.dgvMyGames.RowHeadersVisible = false;
            this.dgvMyGames.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMyGames.Size = new System.Drawing.Size(460, 120);
            this.dgvMyGames.TabIndex = 0;
            this.dgvMyGames.SelectionChanged += new System.EventHandler(this.dgvMyGames_SelectionChanged);
            // 
            // lblMyGames
            // 
            this.lblMyGames.AutoSize = true;
            this.lblMyGames.Location = new System.Drawing.Point(9, 34);
            this.lblMyGames.Name = "lblMyGames";
            this.lblMyGames.Size = new System.Drawing.Size(152, 13);
            this.lblMyGames.TabIndex = 1;
            this.lblMyGames.Text = "Игры, в которых я участвую:";
            // 
            // tsMain
            // 
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbAddGame,
            this.tsbDeleteGame,
            this.tsbConfigGame,
            this.tsbSaves,
            this.tsbRefresh,
            this.tsbUploadFirstSave});
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(471, 25);
            this.tsMain.TabIndex = 2;
            this.tsMain.Text = "toolStrip1";
            // 
            // tsbAddGame
            // 
            this.tsbAddGame.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbAddGame.Image = ((System.Drawing.Image)(resources.GetObject("tsbAddGame.Image")));
            this.tsbAddGame.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbAddGame.Name = "tsbAddGame";
            this.tsbAddGame.Size = new System.Drawing.Size(23, 22);
            this.tsbAddGame.Text = "Добавить игру";
            this.tsbAddGame.Click += new System.EventHandler(this.tsbAddGame_Click);
            // 
            // tsbDeleteGame
            // 
            this.tsbDeleteGame.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDeleteGame.Image = ((System.Drawing.Image)(resources.GetObject("tsbDeleteGame.Image")));
            this.tsbDeleteGame.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDeleteGame.Name = "tsbDeleteGame";
            this.tsbDeleteGame.Size = new System.Drawing.Size(23, 22);
            this.tsbDeleteGame.Text = "Удалить игру";
            // 
            // tsbConfigGame
            // 
            this.tsbConfigGame.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbConfigGame.Image = ((System.Drawing.Image)(resources.GetObject("tsbConfigGame.Image")));
            this.tsbConfigGame.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbConfigGame.Name = "tsbConfigGame";
            this.tsbConfigGame.Size = new System.Drawing.Size(23, 22);
            this.tsbConfigGame.Text = "Настроить игру";
            this.tsbConfigGame.Click += new System.EventHandler(this.tsbConfigGame_Click);
            // 
            // tsbSaves
            // 
            this.tsbSaves.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbSaves.Image = ((System.Drawing.Image)(resources.GetObject("tsbSaves.Image")));
            this.tsbSaves.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbSaves.Name = "tsbSaves";
            this.tsbSaves.Size = new System.Drawing.Size(23, 22);
            this.tsbSaves.Text = "Сэйвы";
            // 
            // tsbRefresh
            // 
            this.tsbRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
            this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefresh.Name = "tsbRefresh";
            this.tsbRefresh.Size = new System.Drawing.Size(23, 22);
            this.tsbRefresh.Text = "Обновить";
            this.tsbRefresh.Click += new System.EventHandler(this.tsbRefresh_Click);
            // 
            // tsbUploadFirstSave
            // 
            this.tsbUploadFirstSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbUploadFirstSave.Image = ((System.Drawing.Image)(resources.GetObject("tsbUploadFirstSave.Image")));
            this.tsbUploadFirstSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbUploadFirstSave.Name = "tsbUploadFirstSave";
            this.tsbUploadFirstSave.Size = new System.Drawing.Size(23, 22);
            this.tsbUploadFirstSave.Text = "Загрузить первый сейв";
            this.tsbUploadFirstSave.Click += new System.EventHandler(this.tsbUploadFirstSave_Click);
            // 
            // frmGamesSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 176);
            this.Controls.Add(this.tsMain);
            this.Controls.Add(this.lblMyGames);
            this.Controls.Add(this.dgvMyGames);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmGamesSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройка совместных игр";
            this.Load += new System.EventHandler(this.frmGamesSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMyGames)).EndInit();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMyGames;
        private System.Windows.Forms.Label lblMyGames;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton tsbAddGame;
        private System.Windows.Forms.ToolStripButton tsbDeleteGame;
        private System.Windows.Forms.ToolStripButton tsbConfigGame;
        private System.Windows.Forms.ToolStripButton tsbSaves;
        private System.Windows.Forms.ToolStripButton tsbRefresh;
        private System.Windows.Forms.ToolStripButton tsbUploadFirstSave;
    }
}