﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MedievalSaveSync
{
    public partial class frmGamesSettings : Form
    {
        int selectedGameId;
        public frmGamesSettings()
        {
            InitializeComponent();
        }

        private void frmGamesSettings_Load(object sender, EventArgs e)
        {
            dgvMyGames.AutoGenerateColumns = true;
            dgvMyGames.DataSource = Game.GetGames(Settings.UserId);
            
        }

        private void tsbAddGame_Click(object sender, EventArgs e)
        {
            string gameName = string.Empty;
            DialogResult dr = DialogResult.OK;
            while (gameName == string.Empty && dr != DialogResult.Cancel)
            {
                dr = InputBox.ShowInputBox("Введите название игры", "Новая игра", ref gameName);
            }
            string saveName = string.Empty;
            while (saveName == string.Empty && dr != DialogResult.Cancel)
            {
                dr = InputBox.ShowInputBox("Введите имя сейв-файла для этой игры", "Имя сейва", ref saveName);
            }
            if (dr == DialogResult.OK)
            {
                int gameId = Game.AddGame(gameName, saveName, Settings.UserId);
                if (gameId == 0)
                {
                    MessageBox.Show("Новая игра не создана");
                }
            }
            tsbRefresh.PerformClick();
        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            dgvMyGames.Columns.Clear();
            dgvMyGames.AutoGenerateColumns = true;
            dgvMyGames.DataSource = Game.GetGames(Settings.UserId);
            dgvMyGames.Columns["CreatorId"].Visible = false;
        }

        private void dgvMyGames_SelectionChanged(object sender, EventArgs e)
        {
            //Отображение иконочек настреок в зависимостиот игры
            if (dgvMyGames.SelectedRows.Count == 1)
            {
                selectedGameId = int.Parse(dgvMyGames.SelectedRows[0].Cells["id"].Value.ToString());
            }
            else
            {
                selectedGameId = 0;
            }
        }

        private void tsbUploadFirstSave_Click(object sender, EventArgs e)
        {
            var game = new Game(selectedGameId);
            if (File.Exists(game.SavePath))
            {
                game.UploadSave();
            }
            else
            {
                MessageBox.Show(String.Format("Нет сейв-файла с именем {0}. Сначала надо сохранить игру в него!", game.SaveName), "Нет сейва", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void tsbConfigGame_Click(object sender, EventArgs e)
        {
            var game = new Game(selectedGameId);
            frmConfigGame fConfigGame = new frmConfigGame(game);
            fConfigGame.StartPosition = FormStartPosition.CenterParent;
            if (fConfigGame.ShowDialog() == DialogResult.OK)
            {
                game.UpdatePlayersList(fConfigGame.Players);
            }
        }
    }
}
