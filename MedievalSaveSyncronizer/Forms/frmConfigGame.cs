﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MedievalSaveSync
{
    public partial class frmConfigGame : Form
    {
        Game Game;
        public List<Player> Players { private set; get;}

        public frmConfigGame(Game game)
        {
            InitializeComponent();
            Game = game;
            Players = game.GetPlayers();
            foreach (var p in Players)
            {
                lbPlayers.Items.Add(p.Name);
            }
            txtSaveName.Text = Game.SaveName;
        }

        private void btnAddPlayer_Click(object sender, EventArgs e)
        {
            string name = string.Empty;
            InputBox.ShowInputBox("Добавление нового игрока", "Введите имя игрока", ref name);
            if (name != string.Empty)
            {
                if (Players.Any(p => p.Name == name))
                {
                    MessageBox.Show(String.Format("Игрок с именем '{0}' уже добавлен в игру", name), "Игрок уже существует", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    var user = User.GetUser(name);
                    if (user.ID > 0)
                    {
                        lbPlayers.Items.Add(user.Name);
                        Players.Add(new Player(user.Name, lbPlayers.Items.Count, Game.ID, user.ID));
                    }
                    else
                    {
                        MessageBox.Show(String.Format("Игрок с именем '{0}' не найден", name), "Игрок не найден", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        private void lvPlayers_ItemDrag(object sender, ItemDragEventArgs e)
        {
            DoDragDrop(e.Item, DragDropEffects.Link);
        }

        private void lvPlayers_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Link;
        }

        private void lvPlayers_DragDrop(object sender, DragEventArgs e)
        {
            //Point cp = myList.PointToClient(new Point(e.X, e.Y));
            //ListViewItem dragToItem = myList.GetItemAt(cp.X, cp.Y);
            //int dropIndex = dragToItem.Index;
        }

        private void lbPlayers_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbPlayers.SelectedItem == null) return;
            lbPlayers.DoDragDrop(lbPlayers.SelectedItem, DragDropEffects.Move);
        }

        private void lbPlayers_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void lbPlayers_DragDrop(object sender, DragEventArgs e)
        {
            Point point = lbPlayers.PointToClient(new Point(e.X, e.Y));
            int index = this.lbPlayers.IndexFromPoint(point);
            if (index < 0) index = this.lbPlayers.Items.Count - 1;
            object data = lbPlayers.SelectedItem;
            this.lbPlayers.Items.Remove(data);
            this.lbPlayers.Items.Insert(index, data);
            int i = 1;
            foreach(var item in lbPlayers.Items)
            {
                Players.Find(p => p.Name == item.ToString()).NumberInTurn = i;
                i++;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Hide();
        }
    }
}
