﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MedievalSaveSync
{
    public partial class frmSettings : Form
    {

        public frmSettings()
        {
            InitializeComponent();
            txtIp.Text = Settings.ServerURL;
            numUserId.Value = Settings.UserId;
            numInterval.Value = Settings.CheckInterval;
            //chkDisableNotification.Checked = Settings.DisableNotification;
            //txtSaveName.Text = Settings.SaveName;
            lblGameFolder.Text = Settings.GameFolder;

            //lstGamesSave.Items.AddRange(new object[] { "a", "111" });
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            lblGameFolder.Text = Settings.SelectGameFolder();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Settings.ServerURL = txtIp.Text;
            Settings.UserId = int.Parse(numUserId.Value.ToString());
            Settings.CheckInterval = int.Parse(numInterval.Value.ToString());
            //Settings.DisableNotification = chkDisableNotification.Checked;
            //Settings.SaveName = txtSaveName.Text;
            Settings.SaveSettings();
            DialogResult = DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
