﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace MedievalSaveSync
{
    /// <summary>
    /// Слой работы с сервером
    /// </summary>
    public class WebLayer
    {
        /// <summary>
        /// Методы сервера
        /// </summary>
        public static class Methods
        {
            public static string GetUser = "GetUser.php";
            public static string GetRushes = "GetRushes.php";
            public static string GetGame = "GetGame.php";
            public static string GetMyGames = "GetMyGames.php";
            public static string AddNewGame = "AddNewGame.php";
            public static string AddPlayerToGame = "AddPlayerToGame.php";
            public static string UploadSave = "UploadSave.php";
            public static string GetGamePlayers = "GetGamePlayers.php";
            public static string UpdatePlayer = "UpdatePlayer.php";
            public static string GetSave = "GetSave.php";
            public static string GetNextTurnUserId = "GetNextTurnUserId.php";
            public static string CheckMySave = "CheckMySave.php";
            public static string GetUserName = "GetUserName.php";
            public static string GetLastSaveDate = "GetLastSaveDate.php";
            public static string PushRush = "PushRush.php";
            public static string RegisterPlayer = "RegisterPlayer.php";
        }


        private static string ServerUrl
        {
            get
            {
                return "http://" + Settings.ServerURL + "/";
            }
        }
        private static string GetUrl(string page)
        {
            return ServerUrl + page;
        }

        /// <summary>
        /// Выполняет метод на сервере с указанными параметрами
        /// </summary>
        /// <param name="methodName">имя метода</param>
        /// <param name="parameters">параметры в виде анонимного типа</param>
        /// <returns></returns>
        public static string Execute(string methodName, dynamic parameters = null)
        {
            using (var client = new WebClient())
            {
                if (parameters != null)
                {
                    PropertyInfo[] properties = parameters.GetType().GetProperties();
                    if (properties.Length > 0)
                    {
                        NameValueCollection param = new NameValueCollection();
                        foreach (var p in properties)
                        {
                            param.Add(p.Name, Convert.ToString((p.GetValue(parameters, null))));
                        }
                        client.QueryString = param;
                    }
                }
                var str = client.DownloadString(GetUrl(methodName));
                return str;
            }
        }

        /// <summary>
        /// Выполняет метод на сервере с указанными параметрами
        /// </summary>
        /// <typeparam name="T">Тип, список которого будет возвращён</typeparam>
        /// <param name="methodName">имя метода</param>
        /// <param name="parameters">параметры в виде анонимного типа</param>
        /// <returns>Список объекто типа T</returns>
        public static List<T> Execute<T> (string methodName, dynamic parameters) 
        {
            PropertyInfo[] properties = parameters.GetType().GetProperties();
            using (var client = new WebClient())
            {
                if (properties.Length > 0)
                {
                    NameValueCollection param = new NameValueCollection();
                    foreach (var p in properties)
                    {
                        param.Add(p.Name, Convert.ToString((p.GetValue(parameters, null))));
                    }
                    client.QueryString = param;
                }
                var resultJson = client.DownloadString(String.Format("http://{0}/{1}", Settings.ServerURL, methodName));
                if (resultJson != "[]")
                {
                    return JsonConvert.DeserializeObject<List<T>>(resultJson);
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Отправляет файл на сервер
        /// </summary>
        /// <param name="methodName">метод, принимающий файл</param>
        /// <param name="parameters">параметры в виде анонимного типа</param>
        /// <param name="filePath">загружаемый файл</param>
        /// <returns></returns>
        public static bool UploadFile(string methodName, dynamic parameters, string filePath)
        {
            PropertyInfo[] properties = parameters.GetType().GetProperties();
            using (var client = new WebClient())
            {
                if (properties.Length > 0)
                {
                    NameValueCollection param = new NameValueCollection();
                    foreach (var p in properties)
                    {
                        param.Add(p.Name, Convert.ToString((p.GetValue(parameters, null))));
                    }
                    client.QueryString = param;
                }
                client.Headers.Add("Content-Type", "binary/octet-stream");
                byte[] result = client.UploadFile(GetUrl(methodName), "POST", filePath);
                string response = Encoding.UTF8.GetString(result, 0, result.Length);
                if (response.Contains("1"))
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Получает файл с сервера
        /// </summary>
        /// <param name="methodName">метод, отправляющий файл</param>
        /// <param name="parameters">параметры в виде анонимного типа</param>
        /// <param name="filePath">путь сохраняемого файла</param>
        /// <returns></returns>
        public static void GetFile(string methodName, dynamic parameters, string filePath)
        {
            PropertyInfo[] properties = parameters.GetType().GetProperties();
            using (var client = new WebClient())
            {
                if (properties.Length > 0)
                {
                    NameValueCollection param = new NameValueCollection();
                    foreach (var p in properties)
                    {
                        param.Add(p.Name, Convert.ToString((p.GetValue(parameters, null))));
                    }
                    client.QueryString = param;
                }
                byte[] fileData = client.DownloadData(new Uri(GetUrl(methodName)));
                if (fileData.Length > 0)
                {
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                        fileStream.Write(fileData, 0, fileData.Length);
                }
            }
        }

        /// <summary>
        /// Проверяет доступность сервера
        /// </summary>
        /// <returns>true - если сервер доступен, иначе false</returns>
        public static bool ServerCheckAvailable()
        {
            return ServerCheckAvailable(Settings.ServerURL);
        }

        /// <summary>
        /// Проверяет доступность сервера
        /// </summary>
        /// <param name="url">адрес сервера</param>
        /// <returns>true - если доступен, иначе false</returns>
        public static bool ServerCheckAvailable(string url)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadString(String.Format("http://{0}", url));
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
